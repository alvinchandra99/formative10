use formativedb;
CREATE TABLE ADDRESS(
id INT auto_increment,
street varchar(255),
city varchar(255),
province varchar(255),
country varchar(255),
zipCode int,
primary key(id)
);

CREATE TABLE Manufacturer(
id INT auto_increment,
name varchar(255),
addressId INT,
primary key(id),
FOREIGN KEY(addressId) REFERENCES Address(id)
);

CREATE TABLE Brand(
id int auto_increment,
name varchar(255),
primary key(id)
);

CREATE TABLE product(
id INT auto_increment,
artNumber INT,
name varchar(255),
description text,
manufactureId INT,
brandId INT,
stock INT,
primary key(id),
foreign key (manufactureId) references manufacturer(id),
foreign key (brandId) references brand(id)
);

CREATE TABLE price(
id INT auto_increment,
productId INT,
valutaId INT,
amount INT,
primary key(id),
FOREIGN KEY(productId) references product(id),
FOREIGN KEY(valutaId) references valuta(id)
);

CREATE TABLE valuta(
id INT auto_increment,
code varchar(10),
name varchar(255),
primary key(id)

);