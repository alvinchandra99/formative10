SELECT * from product;

INSERT INTO product (artNumber,name, description, manufactureId, brandId, stock)
VALUES
( 22492883,"Xiaomi Mi 1", "8GB/ 128GB", 1, 1,0),
( 18386639, "Xiaomi Mi 1", "4GB / 64GB", 1, 1, 10),
( 25487466, "Apple X ","4GB / 64GB", 2 ,1, 15),
( 30457171, "Apple X ","4GB / 64GB", 2, 2, 20),
( 65780626, "Apple X ","4GB / 64GB", 2, 1, 10),
( 87497038, "Xiaomi Mi 1", "4GB / 64GB", 1 , 2, 5),
( 24430289, "Apple X ","4GB / 64GB", 2 , 1 , 0),
( 03161388, "Xiaomi Mi 1", "4GB / 64GB", 1 , 1 , 7),
( 94472950, "Apple X ", "4GB / 64GB", 2 , 2 , 5),
( 15393470, "Apple X ","4GB / 64GB", 2 , 1 , 6);

INSERT INTO address (street, city, province, country, zipcode)
VALUES
("Jalan Rasamala", "Batam", "Kepulauan Riau", "Indonesia", 15560),
("Jalan Gatot Subroto" , "Tangerang", "Banten", "Indonesia", 15561);


INSERT INTO manufacturer (name, addressId)
VALUES
("PT Teknologi Indonesia", 2),
("PT Masa Depan", 1);

INSERT INTO brand (name)
VALUES
("Xiaomi"),
("Apple");

INSERT INTO valuta(code, name)
VALUES
("USD", "Dollar"),
("IDR", "Rupiah");

INSERT INTO price(productId, valutaId, amount)
VALUES
(1,1, 500),
(1,2, 6000000),
(2,1, 700),
(2,2, 10000000),
(3,1, 600),
(3,2, 70000000),
(4,1, 800),
(4,2, 10000000),
(5,1, 800),
(5,2, 10000000),
(6,1, 800),
(6,2, 10000000),
(7,1, 800),
(7,2, 10000000),
(8,1, 800),
(8,2, 10000000),
(9,1, 900),
(9,2, 10000000),
(10,1, 800),
(10,2, 10000000);




